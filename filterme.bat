@echo off

setlocal enabledelayedexpansion

for %%p in (%*) do set path=%~dp1

set filter=

set /p filter=Please give the filter you wish to apply:

set /A count=0
for %%a in (%*) do (

	echo Processing...%%a

	"%ProgramFiles%\Wireshark\tshark.exe" -r %%a -w "%path%temp1" -R "%filter%"
		
	if !count! == 0 (
		move "%path%temp1" "%path%merged"
	)
	
	if not !count! == 0 (
		move "%path%merged" "%path%temp2"
		
		"%ProgramFiles%\Wireshark\mergecap.exe" -w "%path%merged" "%path%temp1" "%path%temp2"
			
		del  "%path%temp1" "%path%temp2"
		
	)

	set /A count+=1
	
)

move "%path%merged" "%path%merged.pcap"

echo Done! File saved to %path%merged.pcap

pause
