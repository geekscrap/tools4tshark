@echo off

setlocal enabledelayedexpansion

for %%p in (%*) do set path=%~dp1

set filter=

set /p filter=Please give the filter you wish to apply:

for %%a in (%*) do (

	echo Processing...%%a

	"C:\Program Files\Wireshark\tshark.exe" -r %%a -Tfields -e tcp.data -e udp.data -e data -R "%filter%" >> %path%tmp.txt
	
	echo. >> %path%tmp.txt

)

start %path%tmp.txt

echo Done! Frame data saved to %path%tmp.txt

pause