@echo off

setlocal enabledelayedexpansion

set filter=dns.qry.name and udp.dstport == 53

for %%p in (%*) do set path=%~dp1

echo.
if (%1) NEQ () (

	for %%a in (%*) do (
		echo Processing... %%a
		"%ProgramFiles%\Wireshark\tshark.exe" -r %%a -nn -Tfields -e frame.time -e eth.src -e dns.qry.name -R "%filter%" >> "%path%dns_requests.txt"
	)

	echo Done, DNS results saved to %path%dns_requests.txt
	pause
	exit

)

:question
set /p toTXT=Do you want the output straight to txt? [y/n]:

if "%toTXT%" NEQ "y" (
	if "%toTXT%" NEQ "n" (	
		goto question
))

set /A count=0

for /f "tokens=*" %%a in ('dir /b "%~dp0*.pcap" ') do (

if %%a NEQ dns.pcap (

	echo Processing... %%a
	
	if "%toTXT%" EQU "y" (
	
	"%ProgramFiles%\Wireshark\tshark.exe" -r "%~dp0%%a" -nn -Tfields -e frame.time -e eth.src -e dns.qry.name -R "%filter%" >> "%~dp0dns_requests.txt"	
	
	) else (
	
		"%ProgramFiles%\Wireshark\tshark.exe" -r "%~dp0%%a" -w "%~dp0temp1" -R "%filter%" 
	
		if !count! == 0 (
			move "%~dp0temp1" "%~dp0merged"
		)
	
		if not !count! == 0 (
			move "%~dp0merged" "%~dp0temp2"
		
		"%ProgramFiles%\Wireshark\mergecap.exe" -w "%~dp0merged" "%~dp0temp1" "%~dp0temp2"
			
		del "%~dp0temp1" "%~dp0temp2"
		
		)
	)
	
	set /A count+=1

)

)

echo.

if "%toTXT%" EQU "n" (
	move "%~dp0merged" "%~dp0dns.pcap"
	echo Done, file saved to %~dp0dns.pcap
) else (
	echo Done, text file saved to %~dp0dns_requests.txt
)

pause

exit
