@echo off

setlocal enabledelayedexpansion

for %%p in (%*) do set path=%~dp1

echo What do you want to sort the DNS queries by?:
echo [d] Date/time
echo [m] MAC
echo [q] DNS query (fastest)

set /p sortby=Please input choice [d, m or q]: 

for %%a in (%*) do (

	if "%sortby%" EQU "d" (
		"%SystemRoot%\System32\sort" %%a /+4 > "%path%dns_requests_time_sorted.txt"
		echo Done, sorted by time and saved to %path%dns_requests_time_sorted.txt
		pause
	)
	
	if "%sortby%" EQU "m" (
		"%SystemRoot%\System32\sort" %%a /+38 > "%path%dns_requests_MAC_sorted.txt"
		echo Done, sorted by MAC and saved to %path%dns_requests_MAC_sorted.txt
		pause
	)
	
	if "%sortby%" EQU "q" (
		"%SystemRoot%\System32\sort" %%a /+50 > "%path%dns_requests_query_sorted.txt"
		echo Done, sorted by query and saved to %path%dns_requests_query_sorted.txt
		pause
	)

)

