@echo off

for %%a in (%*) do set path=%~dp1

echo Processing...

"%ProgramFiles%\Wireshark\mergecap.exe" -w "%path%merged.pcap" %*

echo Done! File saved to %path%merged.pcap

pause
