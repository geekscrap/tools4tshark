#!/usr/bin/python

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import geoip2.database

# Customise ##############################

mmdb	= "/root/tmp/GeoLite2-City.mmdb"
iplist	= "/root/tmp/iplist.txt"

# Dot alpha - Set to 1 if you dont want alpha
alphaval=0.2
# Landmass colour
rgb=(0,0,0)

print "Drawing map..."

# Map, uncomment/comment as required
m = Basemap(projection='cyl', llcrnrlat=-80, urcrnrlat=80, llcrnrlon=-180, urcrnrlon=180, lat_ts=20)
m.drawcoastlines()
m.drawcountries()
m.fillcontinents(color = rgb)
m.drawmapboundary()
#m.bluemarble()

# Do we want a location graph or a heatmap?
heatmap = True
# Dot Scaling factor
scale=2

# Customise ##############################

reader = geoip2.database.Reader(mmdb)
hits = []

with open(iplist, "r") as l:
	for ip in l:

		if not ip: continue
		ip = ip.rstrip()

		try:
			response = reader.city(ip)
			country	= str(response.country.name)
			city	= str(response.city.name)
			lat	= response.location.latitude
			lon	= response.location.longitude

			print ("%s, %s, %s, %s, %s" % (ip, country, city, str(lat), str(lon)))

			hits.append((ip, country, city, lat, lon))

			if not heatmap:
				m.plot(lon, lat, 'ro', markersize=5, alpha=alphaval)

		except:
			True

if not heatmap:
	plt.show()
	exit()

i = 0
comp = []

while i < len(hits):
	ip, country, city, lat, lon = hits[i]

	if not (lat,lon) in comp:
		
		count = 0
		print "Comparing: " + str(lat), str(lon)

		for y in hits:
			ip2, country2, city2, lat2, lon2 = y

			if (lat,lon) == (lat2,lon2):
				print 'Match'
				count += 1

		m.plot(lon, lat, 'ro', markersize=(count/scale), alpha=alphaval)

	comp.append((lat,lon))
	i += 1

plt.show()
