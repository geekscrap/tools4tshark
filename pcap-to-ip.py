#!/usr/bin/python
import socket, sys

if not (len(sys.argv[1:]) > 0):
        print('No files!')
        input()
        sys.exit(1)

ipadd = input('The IP address to fire the pcap at: ')
port = int(input('Port number: '))

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((ipadd, port))

for pcap in sys.argv[1:]:
        print('Sending: ' + pcap)
        file = open(pcap, 'rb')
        data = file.read()
        file.close()
        
        s.send(data)

        print('Sent!\n')

s.close()

input('Press Enter to quit')
